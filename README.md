## Matthew's README

Welcome to my README! I'm [Matthew](https://www.linkedin.com/in/matthew-macfarlane/), Product Manager for Knowledge Group at GitLab.

## Get to know me

- I was born in Montana and now call Seattle home. I enjoy trail running, biking up steep hills, and most importantly, spending time with my family.

## My tech journey

- I bring a diverse analytical and creative background to my work in technology. At Pacific Lutheran University, I pursued a combination of economics and history, finding joy in both the quantitative precision of macroeconomics and statistics, while honing my research and narrative skills through historical analysis. My journey into tech began at Splunk in sales and marketing. Like many graduates with a hybrid quantitative-qualitative background, I initially struggled to find my place in the industry. However, these customer-facing roles sparked my curiosity about the product development process, or as I like to say, how the "bread is made." This interest led me to transition into product management at GitLab, where I discovered my true calling. My earlier experience working directly with customers proved invaluable, allowing me to effectively translate user needs into product solutions. This combination of customer insight and technical understanding has become my signature strength in product management.

## Working with me

- My work hours are 7:00am to 3:00pm PST.
- I'm an introvert but always open to coffee chats!
- I use GitLab To-do's, Slack, and Gmail to keep everything on track. Feel free to ping me in whatever avenue you prefer!

## My personal values and passions

- Quality time with family and friends
- Trail running
- Night skiing!
- Pizza
- Playing basketball
- Eating thai food
- More outdoor activities!
- Gardening
- Eating more pizza
